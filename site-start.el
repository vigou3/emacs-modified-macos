;;; site-start.el --- Customizations for Emacs Modified for macOS
;;
;; Copyright (C) 2009-2025 Vincent Goulet
;;
;; Emacs Modified for macOS is free software; you can redistribute it
;; and/or modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.
;;
;; Author: Vincent Goulet
;;
;; This file is part of Emacs Modified for macOS
;; https://gitlab.com/emacs-modified/emacs-modified--macos

;;;
;;; Environment setting
;;;
;; By default, environment variables PATH, MANPATH, LANG, TEXINPUTS
;; and BIBINPUTS are imported from the shell at startup. If needed,
;; more variables may be imported by listing them in
;; 'exec-path-from-shell-variables' variable. You may also Customize
;; this variable.
;;
;; Customizations should probably go in a user's .emacs file, so just
;; consider the code below as an example of how to do it. Bear in mind
;; that LANG, TEXINPUTS and BIBINPUTS are added to
;; 'exec-path-from-shell-variables' in default.el, so there is no need
;; to include them in the definition.
;(setq exec-path-from-shell-variables '("PATH" "MANPATH" "FOO" "BAR"))

;;;
;;; Nice options to have On by default
;;;
(mouse-wheel-mode t)			; activate mouse scrolling
(global-font-lock-mode t)		; syntax highlighting
(transient-mark-mode t)			; sane select (mark) mode
(delete-selection-mode t)		; entry deletes marked text
(show-paren-mode t)			; match parentheses
(add-hook 'text-mode-hook 'turn-on-auto-fill) ; wrap long lines in text mode

;;;
;;; AUCTeX
;;;
;; Turn on RefTeX for LaTeX documents. Put further RefTeX
;; customizations in your .emacs file.
(add-hook 'LaTeX-mode-hook
	  (lambda ()
	    (turn-on-reftex)
	    (setq reftex-plug-into-AUCTeX t)))

;; Defensive hack to find latex in case the PATH environment variable
;; was not correctly altered at TeX Live installation. Contributed by
;; Rodney Sparapani <rsparapa@mcw.edu>.
(require 'executable)
(if (and (not (executable-find "latex")) (file-exists-p "/usr/texbin"))
    (setq LaTeX-command-style
	  '(("" "/usr/texbin/%(PDF)%(latex) %S%(PDFout)"))))

;;;
;;; ESS
;;;
;; Following the "source is real" philosophy put forward by ESS, one
;; should not save the workspace at the end of an R session. Hence,
;; the option is disabled here.
(setq-default inferior-R-args "--no-save ")

;; Automagically delete trailing whitespace when saving R script
;; files.
(add-hook 'ess-mode-hook
	  (lambda ()
	     (add-hook 'write-contents-functions
		       (lambda ()
                         (ess-nuke-trailing-whitespace)))
	     (setq ess-nuke-trailing-whitespace-p t)))

;;;
;;; polymode
;;;
;; Temporary (?) fix for the interaction between polymode 0.2.2 and
;; AUCTeX >= 14.0.0. Contributed by Ramon Diaz-Uriarte
;; (https://github.com/polymode/polymode/issues/336)
(with-eval-after-load 'polymode
  (define-hostmode poly-latex-hostmode :mode 'LaTeX-mode)
  )

;;;
;;; tabbar
;;;
;; Start with (tabbar-mode) in ~/.emacs
;; Toggle with (tabbar-mode -1)
;; tabbar v2.2 was cloned from https://github.com/dholm/tabbar
;; tabbar-mode's inclusion in Vincent's distribution is maintained by 
;; Rodney Sparapani <rsparapa@mcw.edu>
(add-to-list 'load-path 
	     (let ((default-directory
		     (file-name-directory load-file-name)))
	       (expand-file-name "tabbar")))
(require 'tabbar) 
