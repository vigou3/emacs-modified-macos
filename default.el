;;; default.el --- Default configuration for Emacs Modified for macOS
;;
;; Copyright (C) 2009-2025 Vincent Goulet
;;
;; Emacs Modified for macOS is free software; you can redistribute it
;; and/or modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.
;;
;; Author: Vincent Goulet
;;
;; This file is part of Emacs Modified for macOS
;; https://gitlab.com/emacs-modified/emacs-modified-macos

;;;
;;; Version number of Emacs Modified for macOS
;;;
;; Define variable and function 'emacs-modified-version'
(require 'version-modified)

;;;
;;; Customize the package system and initialize packages
;;;
(require 'package)
(add-to-list 'package-directory-list
	     (concat (file-name-parent-directory lisp-directory)
		     "site-lisp/elpa"))
(add-to-list 'package-archives
	     '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

;;;
;;; Import the shell environment
;;;
;; Import some shell environment variables into Emacs at launch. Steve
;; Purcell's exec-path-from-shell imports PATH and MANPATH by default;
;; LANG, TEXINPUTS and BIBINPUTS are added here. You can customize
;; 'exec-env-from-shell-variables' in site-start.el or the user's
;; config file.
(require 'exec-path-from-shell)
(nconc exec-path-from-shell-variables '("LANG" "TEXINPUTS" "BIBINPUTS"))
(exec-path-from-shell-initialize)
