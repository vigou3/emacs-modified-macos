### User settable variables used in -*-Makefile-*-
##
## Copyright (C) 2009-2025 Vincent Goulet
##
## Emacs Modified for macOS is free software; you can redistribute it
## and/or modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
##
## GNU Emacs is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with GNU Emacs; see the file COPYING.  If not, write to the
## Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
## Boston, MA 02110-1301, USA.
##
## Author: Vincent Goulet
##
## This file is part of Emacs Modified for macOS
## https://gitlab.com/emacs-modified/emacs-modified-macos

## GNU Emacs and this distribution version number
EMACSVERSION := 30.1
EMACSPATCHLEVEL := 
DISTVERSION := 1

## Packages version numbers
ESSVERSION := 25.1.0
AUCTEXVERSION := 14.0.9
POLYMODEVERSION := 0.2.2
MARKDOWNMODEVERSION := 2.7
TABBARVERSION := 2.2
EXECPATHVERSION := 2.2
DICT-ENVERSION := 2025.01.01
DICT-EN-ID := 41/1735709291
DICT-FRVERSION := 5.7
DICT-DEVERSION := 2017.01.12
DICT-ESVERSION := 2.9
DICT-ES-ID := 98/1735858493

## Name of the Apple Developer ID Application certificate used to
## codesign the application
DEVELOPERID := Vincent Goulet

## Entries for Apple notarization service.
APPLEID := vincent.goulet@me.com
TEAMID := 8WW8DC8M46

## GitLab repository and authentication
REPOSURL := https://gitlab.com/emacs-modified/emacs-modified-macos
REPOSNAME := $(shell basename ${REPOSURL})
APIURL := https://gitlab.com/api/v4/projects/emacs-modified%2F${REPOSNAME}
OAUTHTOKEN := $(shell cat ~/.gitlab/token)

### No modification should be necessary beyond this point

## Version strings and file names
VERSION := ${EMACSVERSION}$(if ${EMACSPATCHLEVEL},-${EMACSPATCHLEVEL},)-modified-${DISTVERSION}
TAGNAME := v${VERSION}
DISTNAME := Emacs-${VERSION}
DMGFILE := Emacs-${EMACSVERSION}$(if ${EMACSPATCHLEVEL},-${EMACSPATCHLEVEL},)-universal.dmg
README := README.txt
NEWS := NEWS

## Base name of local packages
TABBAR := tabbar-${TABBARVERSION}
DICT-EN := dict-en-$(subst .,,${DICT-ENVERSION})_lo
DICT-FR := lo-oo-ressources-linguistiques-fr-v$(subst .,-,${DICT-FRVERSION})
DICT-DE := dict-de-de-frami-$(subst .,-,${DICT-DEVERSION})
DICT-ES := es

## DMG mount point
VOLUME := /Volumes
