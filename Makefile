### -*-Makefile-*- to build Emacs Modified for macOS
##
## Copyright (C) 2009-2025 Vincent Goulet
##
## Emacs Modified for macOS is free software; you can redistribute it
## and/or modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
##
## GNU Emacs is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with GNU Emacs; see the file COPYING.  If not, write to the
## Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
## Boston, MA 02110-1301, USA.
##
## Author: Vincent Goulet
##
## This file is part of Emacs Modified for macOS
## https://gitlab.com/emacs-modified/emacs-modified-macos

## Set most variables from the appropriate Makeconf file
include ./Makeconf

## Build directory et al.
BUILDDIR := ${CURDIR}/builddir
BUILDDMG := ${CURDIR}/tmpdmg.dmg
EMACSDIR := ${BUILDDIR}/Emacs.app
DICTDIR := ${BUILDDIR}/Dictionaries

## Emacs specific info.
PREFIX := ${EMACSDIR}/Contents
EMACS := ${PREFIX}/MacOS/Emacs
EMACSBATCH := ${EMACS} -batch -no-site-file -no-init-file

## Destination directories. The default macOS official site-lisp
## directory is now in /Library. To keep the distribution
## self-contained, the init files and local additions (other than the
## packages) are rather stored in Resources/lisp. Adding
## Resources/site-lisp to load-path would put it in front user
## additions, which is not playing nice.
DESTDIR := ${PREFIX}/Resources
SITELISP := ${DESTDIR}/lisp
ELPADIR := ${DESTDIR}/site-lisp/elpa
DOCDIR = ${DESTDIR}/doc

## Toolset
CP := cp -p
RM := rm -r
MD := mkdir -p
UNZIP := unzip
UNTAR := tar xzf
SVN := /Library/Developer/CommandLineTools/usr/bin/svn # for macOS >= 11 Big Sur

## Packages to include in the distribution
ELPAPACKAGES := ess auctex polymode markdownmode execpath
LOCALPACKAGES := tabbar dict
PACKAGES := ${ELPAPACKAGES} ${LOCALPACKAGES}

all: get-packages emacs

get-packages: get-emacs $(addprefix get-,${LOCALPACKAGES})

emacs: dir icon base-config ${PACKAGES} dmg

dmg: codesign bundle

release: notarize staple check-status create-release upload create-link

.PHONY: dir
dir:
	@echo ----- Creating the application in temporary directory...
	if [ -d ${BUILDDIR} ]; then ${RM} -f ${BUILDDIR}; fi
	hdiutil attach ${DMGFILE} -noautoopen -quiet
	ditto ${VOLUME}/Emacs/Emacs.app ${EMACSDIR}
	hdiutil detach ${VOLUME}/Emacs -quiet

.PHONY: icon
icon:
	@echo ----- Making the icon...
	${MAKE} -C icon

.PHONY: base-config
base-config:
	@echo ----- Putting in place the basic configuration...
	${MD} ${ELPADIR}
	${CP} icon/Emacs.icns ${DESTDIR}/
	${CP} default.el ${SITELISP}/
	${CP} site-start.el ${SITELISP}/
	sed '/^(defconst/s/<DISTVERSION>/${DISTVERSION}/' \
	    version-modified.el.in > ${SITELISP}/version-modified.el
	${EMACSBATCH} -f batch-byte-compile ${SITELISP}/version-modified.el

.PHONY: ess
ess:
	@echo ----- Installing ESS...
	${EMACSBATCH} --eval "(progn (package-refresh-contents) (setq package-user-dir \"${ELPADIR}\") (package-install 'ess))"

.PHONY: auctex
auctex:
	@echo ----- Installing AUCTeX...
	${EMACSBATCH} --eval "(progn (package-refresh-contents) (setq package-user-dir \"${ELPADIR}\") (package-install 'auctex))"

.PHONY: polymode
polymode: markdownmode
	@echo ----- Installing polymode...
	${EMACSBATCH} --eval "(progn (require 'package) (setq package-user-dir \"${ELPADIR}\") (add-to-list 'package-archives '(\"melpa-stable\" . \"https://stable.melpa.org/packages/\") t) (package-refresh-contents) (mapc 'package-install '(poly-R poly-markdown poly-noweb)))"

.PHONY: markdownmode
markdownmode:
	@echo ----- Installing markdown-mode...
	${EMACSBATCH} --eval "(progn (require 'package) (setq package-user-dir \"${ELPADIR}\") (add-to-list 'package-archives '(\"melpa-stable\" . \"https://stable.melpa.org/packages/\") t) (package-refresh-contents) (package-install 'markdown-mode))"

.PHONY: execpath
execpath:
	@echo ----- Installing exec-path-from-shell...
	${EMACSBATCH} --eval "(progn (require 'package) (setq package-user-dir \"${ELPADIR}\") (add-to-list 'package-archives '(\"melpa-stable\" . \"https://stable.melpa.org/packages/\") t) (package-refresh-contents) (package-install 'exec-path-from-shell))"

## Do NOT byte-compile tabbar
.PHONY: tabbar
tabbar:
	@echo ----- Making tabbar...
	if [ -d ${TABBAR} ]; then ${RM} -f ${TABBAR}; fi
	${UNZIP} ${TABBAR}.zip
	${MD} ${SITELISP}/tabbar
	${CP} ${TABBAR}/*.el ${SITELISP}/tabbar
	${CP} ${TABBAR}/*.tiff ${SITELISP}/tabbar
	${CP} ${TABBAR}/*.png ${SITELISP}/tabbar
	${MD} ${DOCDIR}/tabbar
	${CP} ${TABBAR}/README.markdown ${DOCDIR}/tabbar/README.md
	${RM} -f ${TABBAR}

.PHONY: dict
dict:
	@echo ----- Installing dictionaries...
	if [ -d ${DICTDIR} ]; then ${RM} -f ${DICTDIR}; fi
	${MD} ${DICTDIR}
	${UNZIP} -j -d ${DICTDIR} ${DICT-EN}.zip "*.aff" "*.dic" "th_en*" "README*en*.txt"
	${UNZIP} -j -d ${DICTDIR} ${DICT-FR}.zip dictionaries/* 
	${UNZIP} -j -d ${DICTDIR} ${DICT-DE}.zip "de_DE_frami/*.aff" "de_DE_frami/*.dic" "de_DE_frami/*README.txt" "hyph_de_DE/*.dic" "hyph_de_DE/*README.txt" "thes_de_DE_v2/th_de_DE*"
	${UNZIP} -j -d ${DICTDIR} ${DICT-ES}.zip "*.aff" "*.dic" "th_es*" "README*es*.txt"

.PHONY: codesign
codesign:
	@echo ----- Signing the application...
	codesign --force --sign "Developer ID Application: ${DEVELOPERID}" \
		 --options=runtime --deep \
	         ${EMACSDIR}

.PHONY: bundle
bundle:
	@echo ----- Creating the disk image...
	if [ -e ${BUILDDMG} ]; then ${RM} ${BUILDDMG}; fi
	hdiutil create ${BUILDDMG} \
		-size 400m \
	 	-format UDRW \
		-fs HFS+ \
		-srcfolder ${BUILDDIR} \
		-volname ${DISTNAME} \
		-quiet

	@echo ----- Mounting the disk image...
	hdiutil attach ${BUILDDMG} -noautoopen -quiet

	@echo ----- Populating top level image directory...
	sed -E -i "" \
	    -e 's/(GNU Emacs )[0-9.]+/\1${EMACSVERSION}/' \
	    -e 's/(ESS )[0-9a-z.]+/\1${ESSVERSION}/' \
	    -e 's/(AUCTeX )[0-9.]+/\1${AUCTEXVERSION}/' \
	    -e 's/(polymode )[0-9.]+/\1${POLYMODEVERSION}/' \
	    -e 's/(markdown-mode.el )[0-9.]+/\1${MARKDOWNMODEVERSION}/' \
	    -e 's/(Tabbar )[0-9.]+/\1${TABBARVERSION}/' \
	    -e 's/(exec-path-from-shell.el )[0-9.]+/\1${EXECPATHVERSION}/' \
	    -e 's/(English \(version )[0-9a-z.]+/\1${DICT-ENVERSION}/' \
	    -e 's/(French \(version )[0-9.]+/\1${DICT-FRVERSION}/' \
	    -e 's/(German \(version )[0-9.]+/\1${DICT-DEVERSION}/' \
	    -e 's/(Spanish \(version )[0-9.]+/\1${DICT-ESVERSION}/' \
	    ${README} && \
	  ${CP} ${README} ${VOLUME}/${DISTNAME}
	${CP} ${NEWS} ${VOLUME}/${DISTNAME}
	ln -s /Applications ${VOLUME}/${DISTNAME}/Applications

	@echo ----- Unmounting and compressing the disk image...
	hdiutil detach ${VOLUME}/${DISTNAME} -quiet
	if [ -e ${DISTNAME}.dmg ]; then ${RM} ${DISTNAME}.dmg; fi
	hdiutil convert ${BUILDDMG} \
		-format UDZO \
		-imagekey zlib-level=9 \
		-o ${DISTNAME}.dmg -quiet

	@echo ----- Cleaning up...
	${RM} -f ${BUILDDIR}
	${RM} -f ${BUILDDMG}

.PHONY: notarize
notarize:
	@echo ----- Notarizing the application...
	xcrun notarytool submit "${DISTNAME}.dmg" \
	                 --wait \
	                 --apple-id "${APPLEID}" \
	                 --team-id "${TEAMID}" \
	                 --keychain-profile "notarytool"

.PHONY: staple
staple:
	@echo ----- Stapling ticket to the application...
	xcrun stapler staple "${DISTNAME}.dmg"

.PHONY: check-status
check-status:
	@{ \
	    printf "%s" "----- Checking status of working directory... "; \
	    branch=$$(git branch --list | grep ^* | cut -d " " -f 2-); \
	    if [ "$${branch}" != "master"  ] && [ "$${branch}" != "main" ]; \
	    then \
	        printf "\n%s\n" "not on branch master or main"; exit 2; \
	    fi; \
	    if [ -n "$$(git status --porcelain | grep -v '^??')" ]; \
	    then \
	        printf "\n%s\n" "uncommitted changes in repository; not creating release"; exit 2; \
	    fi; \
	    if [ -n "$$(git log origin/master..HEAD | head -n1)" ]; \
	    then \
	        printf "\n%s\n" "unpushed commits in repository; pushing to origin"; \
	        git push; \
	    else \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: create-release
create-release:
	@{ \
	    printf "%s" "----- Checking if a release already exists... "; \
	    http_code=$$(curl -I ${APIURL}/releases/${TAGNAME} 2>/dev/null \
	                     | head -n1 | cut -d " " -f2) ; \
	    if [ "$${http_code}" = "200" ]; \
	    then \
	        printf "%s\n" "yes"; \
	        printf "%s\n" "using the existing release"; \
	    else \
	        printf "%s\n" "no"; \
	        printf "%s" "Creating release on GitLab... "; \
	        name=$$(awk '/^# / { sub(/# +VERSION +/, "", $$0); print $$0; exit }' ${NEWS}); \
	        desc=$$(awk ' \
	                      /^$$/ { next } \
	                      (state == 0) && /^# / { state = 1; next } \
	                      (state == 1) && /^# / { exit } \
	                      (state == 1) { print } \
	                    ' ${NEWS}); \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --output /dev/null --silent \
	             "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master" && \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --data tag_name="${TAGNAME}" \
	             --data name="Emacs Modified for macOS $${name}" \
	             --data description="$${desc}" \
	             --output /dev/null --silent \
	             ${APIURL}/releases; \
	        printf "%s\n" "done"; \
	    fi; \
	}

.PHONY: upload
upload:
	@printf "%s\n" "----- Uploading package to registry..."
	curl --upload-file "${DISTNAME}.dmg" \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --silent \
	     "${APIURL}/packages/generic/Emacs-Modified-macOS/${VERSION}/${DISTNAME}.dmg"

.PHONY: create-link
create-link: create-release
	@printf "%s\n" "----- Adding package link to the release... "; \
	$(eval pkg_id=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                           --silent \
	                           "${APIURL}/packages" \
	                      | grep -o -E '\{[^{]*"version":"${VERSION}"[^}]*}' \
	                      | grep -o '"id":[0-9]*' | cut -d: -f 2))
	$(eval file_id=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                            --silent \
	                            "${APIURL}/packages/${pkg_id}/package_files" \
	                       | grep -o -E '\{[^{]*"file_name":"${DISTNAME}\.dmg"[^}]*}' \
	                       | grep -o '"id":[0-9]*' | cut -d: -f 2))
	@printf "%s: %s\n" "package file id" "${file_id}"
	curl --request POST \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --data name="${DISTNAME}.dmg" \
	     --data url="${REPOSURL:/=}/-/package_files/${file_id}/download" \
	     --data link_type="package" \
	     --output /dev/null --silent \
	     "${APIURL}/releases/${TAGNAME}/assets/links"

.PHONY: get-emacs
get-emacs:
	@echo ----- Fetching Emacs...
	if [ -f ${DMGFILE} ]; then ${RM} ${DMGFILE}; fi
	curl -O -L https://emacsformacosx.com/emacs-builds/${DMGFILE}

.PHONY: get-tabbar
get-tabbar:
	@echo ----- Fetching tabbar...
	if [ -f ${TABBAR}.zip ]; then ${RM} ${TABBAR}.zip; fi
	curl -OL https://github.com/dholm/tabbar/archive/v${TABBARVERSION}.zip
	${CP} v${TABBARVERSION}.zip ${TABBAR}.zip
	${RM} v${TABBARVERSION}.zip

.PHONY: get-dict
get-dict:
	@echo ----- Fetching dictionaries
	if [ -f ${DICT-EN}.zip ]; then ${RM} ${DICT-EN}.zip; fi
	curl -L -o ${DICT-EN}.zip https://extensions.libreoffice.org/assets/downloads/${DICT-EN-ID}/${DICT-EN}.oxt
	if [ -f ${DICT-FR}.zip ]; then ${RM} ${DICT-FR}.zip; fi
	curl -L -o ${DICT-FR}.zip https://extensions.libreoffice.org/assets/downloads/z/${DICT-FR}.oxt
	if [ -f ${DICT-DE}.zip ]; then ${RM} ${DICT-DE}.zip; fi
	curl -L -o ${DICT-DE}.zip https://extensions.libreoffice.org/assets/downloads/z/${DICT-DE}.oxt
	if [ -f ${DICT-ES}.zip ]; then ${RM} ${DICT-ES}.zip; fi
	curl -L -o ${DICT-ES}.zip https://extensions.libreoffice.org/assets/downloads/${DICT-ES-ID}/${DICT-ES}.oxt

.PHONY: clean
clean:
	${RM} ${BUILDDIR}
	${RM} ${DISTNAME}.dmg
