# Emacs Modified for macOS icon

The Emacs icon in this directory is based on the [macOS Big Sur Emacs icon](https://www.figma.com/file/sWvalMNLeDrzzqM8zS4gON/Emacs-Big-Sur-icons?node-id=0%3A1) created by [elrumo](https://github.com/elrumo) for [macOSicons](https://macosicons.com). The inclined logo is inspired by the macOS Yosemite icon created by Thomas Kobber Panum that was used for the distribution from 2014 to 2023.

The source code of the icon is hosted on [Figma](https://www.figma.com/file/9m9LgTH4zm5yRYAEEhnLOu/macOS-Big-Sur-Emacs-icon-(Copy)?type=design&node-id=1-8&mode=design&t=zgxYRTPAJxu60pw5-0).

## Creation of an icon file

See the `Makefile` to transform the `.png` image into a macOS `.icns` file.

## License

The icon in this directory is licensed under the GNU General Public License as published by the Free Software Foundation; either version 3, or (at your option) any later version.

